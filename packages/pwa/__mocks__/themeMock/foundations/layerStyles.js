/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
const tile = {
    // py: 6,
    // px: 4,
    // backgroundColor: 'none',
    // rounded: 'base',
    // boxShadow: 'base'
}

const tileBordered = {
    ...card
    // px: [4, 4, 5, 6],
    // border: '1px solid',
    // borderColor: 'gray.100'
}

export default {
    tile,

    tileBordered,

    page: {
        // px: [4, 4, 6, 6, 8],
        // paddingTop: [4, 4, 6, 6, 8],
        // paddingBottom: 32,
        // width: '100%',
        // maxWidth: 'container.xxxl',
        // marginLeft: 'auto',
        // marginRight: 'auto'
    }
}
