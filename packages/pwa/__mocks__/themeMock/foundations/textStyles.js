/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
const base = {
    // font-family: "Source Sans Pro"
}

const heading = {
    ...base
}

const text = {
    ...base
}

export default {
    heading,

    text,

    page: {
        // px: [4, 4, 6, 6, 8],
        // paddingTop: [4, 4, 6, 6, 8],
        // paddingBottom: 32,
        // width: '100%',
        // maxWidth: 'container.xxxl',
        // marginLeft: 'auto',
        // marginRight: 'auto'
    }
}
