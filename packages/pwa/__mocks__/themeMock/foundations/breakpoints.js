/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */

import {createBreakpoints} from '@chakra-ui/theme-tools'

export default createBreakpoints({
    //   xs: "30em",
    //   sm: "30em",
    //   md: "48em",
    //   lg: "62em",
    //   xl: "80em",
    //   "2xl": "96em",
})
