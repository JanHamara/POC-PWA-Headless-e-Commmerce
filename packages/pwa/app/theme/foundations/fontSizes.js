/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */

export default {
    base: '1rem', // 16px
    '3xs': '0.75rem', // 12px
    '2xs': '0.875rem', // 14px
    xs: '0.9375rem', // 15px
    sm: '1.125rem', // 18px
    md: '1.25rem', // 20px
    lg: '1.5rem', // 24px
    xl: '1.875rem', // 30px
    '2xl': '2.25rem', // 36px
    '3xl': '3rem', // 48px
    '4xl': '3.75rem' // 60px
}
